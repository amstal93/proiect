import React from 'react';

const QuestionView = ({question, next}) => {
 
    return(
        <div className='questionView'>
            <p className='questionSt'>{question.statement}</p>
            <div className='variante'>
                <button className='box'>{question.firstAnswer}</button>
                <button className='box'>{question.secondAnswer}</button>
                <button className='box'>{question.thirdAnswer}</button>
                <button className='box'>{question.fourthAnswer}</button>
            </div>
        </div>
    )
};

export default QuestionView;