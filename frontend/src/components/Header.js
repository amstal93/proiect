import React from 'react';
import {Link} from 'react-router-dom'
const Header = (props) => {
    const displayButtons = props.displayButtons;
    const logHandler = props.logHandler;
    const isGame = props.action === "game";
    const isLeaderboard = props.action === "leaderboard";
    const isFlagged = props.action === "flagged";
    const isUsers = props.action === "users";

    return(
        <div className='header'>
            <div className='headercontent'>
                <div id="header_title"><b>Trivia 🔥</b></div>
                {
                    displayButtons
                    ?
                    <div id="PageButtons">
                        {
                            isGame ?
                            <Link to='/quiz'>
                                <button>
                                    {localStorage.getItem('ro') === 'true' ? 'Acasa' : 'Home'}
                                </button>
                            </Link>
                            :
                            <Link to='/quiz/game'>
                                <button>
                                    {localStorage.getItem('ro') === 'true' ? 'Incepe Jocul' : 'Start Game'}
                                </button>
                            </Link>
                        }
                        {
                            isLeaderboard ?
                            <Link to='/quiz'>
                                <button>
                                    {localStorage.getItem('ro') === 'true' ? 'Acasa' : 'Home'}
                                </button>
                            </Link>
                            :
                            <Link to='/quiz/leaderboard'>
                                <button>
                                    {localStorage.getItem('ro') === 'true' ? 'Clasament' : 'Leaderboard'}
                                </button>
                            </Link>
                        }
                        {
                            localStorage.getItem('role') === 'MANAGER' && !isFlagged &&
                            <Link to='/quiz/flagged'>
                                <button>
                                    {localStorage.getItem('ro') === 'true' ? 'Cu' : 'With'} Flag
                                </button>
                            </Link>
                        }
                        {
                            localStorage.getItem('role') === 'MANAGER' && isFlagged &&
                            <Link to='/quiz'>
                                <button>
                                    {localStorage.getItem('ro') === 'true' ? 'Acasa' : 'Home'}
                                </button>
                            </Link>
                        }
                        {
                            localStorage.getItem('role') === 'ADMIN' && !isUsers &&
                            <Link to='/quiz/users'>
                                <button>
                                    {localStorage.getItem('ro') === 'true' ? 'Utilizatori' : 'Users'}
                                </button>
                            </Link>
                        }
                        {
                            localStorage.getItem('role') === 'ADMIN' && isUsers &&
                            <Link to='/quiz'>
                                <button>
                                    {localStorage.getItem('ro') === 'true' ? 'Acasa' : 'Home'}
                                </button>
                            </Link>
                        }
                        <Link to='/login'>
                            <button onClick={(e)=>{localStorage.clear(); logHandler(false)}}>
                                {localStorage.getItem('ro') === 'true' ? 'Deconectare' : 'Logout'}
                            </button>
                        </Link>
                    </div>
                    :
                    <div>
                    </div>
                }
            </div>
        </div>
    )
}
export default Header