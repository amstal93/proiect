import './App.scss';
import Layout from './components/Layout.js';
import {HashRouter, Switch, Route, Redirect} from 'react-router-dom'
import { useState } from 'react';

function App() {
  const [isLogged, setLogged] = useState(localStorage.getItem('jwt') != null);
  
  return (
    <div className="App">
      <HashRouter basename='/'>
        <Switch>
          <Route path={'/login'} component={() =>
            isLogged
            ?
            <Redirect to='/quiz' logHandler={setLogged}></Redirect>
            :
            <Layout action='log' logHandler={setLogged}></Layout>
          }/>
          <Route path={'/register'}component={() =>
            isLogged
            ?
            <Redirect to='/quiz' logHandler={setLogged}></Redirect>
            :
            <Layout action='reg' logHandler={setLogged}></Layout>
          }/>
          <Route path={'/quiz/game'}component={() =>
            !isLogged
            ?
            <Redirect to='/login' logHandler={setLogged}></Redirect>
            :
            <Layout action='game' logHandler={setLogged}></Layout>
          }/>
          <Route path={'/quiz/leaderboard'} component={() =>
            <Layout action='leaderboard' logHandler={setLogged}></Layout>
          }/>
          <Route path={'/quiz/flagged'} component={() =>
            <Layout action='flagged' logHandler={setLogged}></Layout>
          }/>
          <Route path={'/quiz/users'} component={() =>
            <Layout action='users' logHandler={setLogged}></Layout>
          }/>
          <Route path={'/leaderboard'} component={() =>
            <Redirect to='/login' logHandler={setLogged}></Redirect>
          }/>
          <Route path={'/quiz'}component={() =>
            !isLogged
            ?
            <Redirect to='/login' logHandler={setLogged}></Redirect>
            :
            <Layout action='quiz' logHandler={setLogged}></Layout>
          }/>
          <Route path={'/'}component={() =>
            isLogged
            ?
            <Redirect to='/quiz' logHandler={setLogged}></Redirect>
            :
            <Redirect to='/login' logHandler={setLogged}></Redirect>
          }/>
          
        </Switch>
      </HashRouter>
    </div>
  );
};

export default App;
