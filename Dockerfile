FROM node:14
WORKDIR /usr/src/app
RUN mkdir ./src
COPY ./package*.json ./
COPY ./Quiz/src ./src
COPY ./.env .
RUN npm ci
RUN ls -R /usr/src/app
CMD ["npm", "run", "start"]