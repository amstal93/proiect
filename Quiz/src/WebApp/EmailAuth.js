const jwt = require('jsonwebtoken');
const nodemailer = require('nodemailer');

const EMAIL_SECRET = process.env.EMAIL_SECRET;

const transport = nodemailer.createTransport({
    service: 'Gmail',
    auth: {
        user: process.env.GMAIL_USER,
        pass: process.env.GMAIL_PASS
    }
});

async function sendEmail(email, userId) {
    const token = jwt.sign(
        { userId },
        EMAIL_SECRET, 
        { expiresIn: '1d' }
    );

    const url = `${process.env.CONFIRM_MAIL_URL}${token}`;

    const mailOptions = {
        from: process.env.GMAIL_USER,
        to: email,
        subject: "Mail Confirmation",
        text: "Plaintext version of the message",
        html: `Please click on this in order to confirm your email: <a href="${url}">${url}</a>`,
    };
    // check .env for email credentials

    transport.sendMail(mailOptions, (error, info) => {
        if (error) {
            return console.log(error);
        }
        //console.log('Message sent: %s', info.messageId, "all info == ", info);
    });
};

module.exports = sendEmail;