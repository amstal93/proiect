const ServerError = require('./ServerError.js');

class FlaggedQuestionPostBody {
    constructor (question_id, user_id) {
        this.userId = parseInt(user_id);
        this.questionId = parseInt(question_id);

        if (!this.userId || this.userId < 1) {
            throw new ServerError("User id should be a positive integer", 400);
        }

        if (!this.questionId || this.questionId < 1) {
            throw new ServerError("Question id should be a positive integer", 400);
        }
    }

    get UserId () {
        return this.userId;
    }

    get QuestionId () {
        return this.questionId;
    }
}

class FlaggedQuestionAddResponse {
    constructor(flaggedQuestion) {
        this.userId = flaggedQuestion.user_id;
        this.questionId = flaggedQuestion.question_id;
    }
}

class FlaggedQuestionGetResponse {
    constructor(flaggedQuestion) {
        this.id = flaggedQuestion.id;
        this.full_statement = flaggedQuestion.full_statement;
        this.a0 = flaggedQuestion.a0;
        this.a1 = flaggedQuestion.a1;
        this.a2 = flaggedQuestion.a2;
        this.a3 = flaggedQuestion.a3;
        this.correct = flaggedQuestion.correct;
        this.nr_flags = flaggedQuestion.nr_flags;
    }
};

module.exports = {
    FlaggedQuestionGetResponse,
    FlaggedQuestionPostBody,
    FlaggedQuestionAddResponse
}