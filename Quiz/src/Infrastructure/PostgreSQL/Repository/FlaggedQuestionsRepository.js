const {
    queryAsync
} = require('..');

const addByIdAsync = async (question_id, user_id) => {
    console.info(`Adding flagged question with id=${question_id} from user with id=${user_id} in database async...`);

    const flagged_questions = await queryAsync('INSERT INTO flagged_questions (question_id, user_id) VALUES ($1, $2) RETURNING *', [question_id, user_id]);
    return flagged_questions[0];
};

const getByIdAsync = async (question_id, user_id) => {
    console.info(`Getting flagged question with id=${question_id} from user with id=${user_id} in database async...`);

    return flagged_questions = await queryAsync('SELECT * FROM flagged_questions FQ WHERE FQ.question_id = $1 AND FQ.user_id = $2', [question_id, user_id]);
};

const getFirstNAsync = async () => {
    const nr_flagged_questions = 10;
    console.info(`Getting first ${nr_flagged_questions} flagged questions from database async...`);

    const flagged_questions = await queryAsync('SELECT Q.id, Q.full_statement, Q.a0, Q.a1, \
                                                       Q.a2, Q.a3, MF.nr_flags, Q.correct \
                                                FROM \
                                                    (SELECT FQ.question_id as question_id, COUNT(FQ.question_id) as nr_flags \
                                                    FROM flagged_questions FQ \
                                                    GROUP BY FQ.question_id \
                                                    FETCH FIRST $1 ROWS ONLY) MF \
                                                INNER JOIN questions Q \
                                                ON Q.id = MF.question_id \
                                                ORDER BY MF.nr_flags DESC', [nr_flagged_questions]);
    return flagged_questions;
};


const deleteByQidUidAsync = async (flagged_question_id, user_id) => {
    console.info(`Deleting flag for question with id=${flagged_question_id} by user ${user_id} from database async...`);

    return await queryAsync('DELETE FROM flagged_questions FQ WHERE FQ.question_id = $1 AND FQ.user_id = $2 RETURNING *', [flagged_question_id, user_id]);
};



const deleteByQidAsync = async (flagged_question_id, good_flag) => {
    const msg_considered = good_flag ? "good" : "bad";
    console.info(`Deleting flagged question with id=${flagged_question_id} considered ${msg_considered} from database async...`);

    const deleted_flagged_questions = await queryAsync('DELETE FROM flagged_questions FQ WHERE FQ.question_id = $1 RETURNING *', [flagged_question_id]);
    const users_who_flagged = deleted_flagged_questions.map(fq => fq.user_id);
    console.info(`Updating flags_score for ${users_who_flagged.length} users async...`);

    if(users_who_flagged.length > 0)
        if (good_flag)
            return await queryAsync(`UPDATE users SET flags_score = flags_score + 1 WHERE id IN (${users_who_flagged.join(", ")}) RETURNING *`);
        else
            return await queryAsync(`UPDATE users SET flags_score = flags_score - 1 WHERE id IN (${users_who_flagged.join(", ")}) RETURNING *`);
};

module.exports = {
    addByIdAsync,
    getByIdAsync,
    getFirstNAsync,
    deleteByQidUidAsync,
    deleteByQidAsync
}
