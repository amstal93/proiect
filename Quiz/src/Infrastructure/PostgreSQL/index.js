const { Pool } = require('pg');
const { getSecret } = require('docker-secret');
const secrets = require('./secrets');


const options = {
  host: process.env.PGHOST,
  database: process.env.PGDATABASE,
  port: process.env.PGPORT,
  user:
    process.env.NODE_ENV === "development"
      ? process.env.PGUSER
      : secrets.read("user-secret"),
  password:
    process.env.NODE_ENV === "development"
      ? process.env.PGPASS
      : secrets.read("password-secret"),
};

const pool = new Pool(options);

const queryAsync = async (text, params) => {
  const start = Date.now();

  const {
    rows,
  } = await pool.query(text, params);
  const duration = Date.now() - start;
  console.log(`Query took ${duration} and returned ${rows.length} rows.`);

  return rows;
};

module.exports = {
  queryAsync,
};
